package org.example.cb.component;

import lombok.RequiredArgsConstructor;
import org.example.cb.model.entity.Privilege;
import org.example.cb.model.entity.Role;
import org.example.cb.model.entity.User;
import org.example.cb.service.PrivilegeService;
import org.example.cb.service.RoleService;
import org.example.cb.service.UserService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.example.cb.component.constant.PrivilegeConstant.BASE;
import static org.example.cb.component.constant.PrivilegeConstant.FULL;

@Component
@RequiredArgsConstructor
public class Initialization implements ApplicationRunner {

    private final UserService userService;
    private final RoleService roleService;
    private final PrivilegeService privilegeService;

    @Override
    public void run(ApplicationArguments args) {

        Privilege full = privilegeService.getByName(FULL);
        Privilege base = privilegeService.getByName(BASE);

        //admin
        Role role = new Role();
        role.setName("ROLE_admin");
        role.setPrivileges(List.of(full));
        roleService.saveRole(role);

        User user = new User();
        user.setUsername("admin");
        user.setPassword("admin");
        user.setRoles(List.of(role));

        userService.saveUser(user);

        //user
        role = new Role();
        role.setName("ROLE_base");
        role.setPrivileges(List.of(base));
        roleService.saveRole(role);

        user = new User();
        user.setUsername("user");
        user.setPassword("user");
        user.setRoles(List.of(role));

        userService.saveUser(user);
    }
}
