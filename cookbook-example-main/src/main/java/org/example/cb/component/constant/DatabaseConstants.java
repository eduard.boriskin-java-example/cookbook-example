package org.example.cb.component.constant;

public interface DatabaseConstants {

        String SCHEMA_COOKBOOK = "cookbook";

        String TABLE_USERS = "users";
        String GEN_USERS = "users_gen";
        String SEQ_USERS = "users_seq";

        String TABLE_PRIVILEGES = "privileges";
        String GEN_PRIVILEGES = "privileges_gen";
        String SEQ_PRIVILEGES = "privileges_seq";

        String TABLE_ROLE = "roles";
        String GEN_ROLES = "roles_gen";
        String SEQ_ROLES = "roles_seq";

        String TABLE_CURRENCY = "currency";

        String CURRENCY_RATE_SEQ = "currency_rate_seq";
        String CURRENCY_RATE_GEN = "currency_rate_gen";


        int SIZE_BATCH = 10;

        String ID = "id";
        String NAME = "name";
        String USERNAME = "username";
        String PASSWORD = "password";
        String LOAD = "load";
        String CODE = "code";

        String USER_ID = "user_id";
        String ROLE_ID = "role_id";
        String PRIVILEGE_ID = "privilege_id";

        String USERS_ROLES = "users_roles";
        String ROLES_PRIVILEGES = "roles_privileges";

        String PRIVILEGES = "privileges";
        String RATE = "rate";
        String DATE = "date";

        String CURRENCY_ID = "currency_id";


}
