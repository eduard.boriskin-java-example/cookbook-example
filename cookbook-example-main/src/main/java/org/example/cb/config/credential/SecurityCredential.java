package org.example.cb.config.credential;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "config.security")
@Getter
@Setter
public class SecurityCredential {

    public int TokenLifespan;
    public int TokenRefreshLifespan;
    private String jwtSecret;
    private String jwtRefreshSecret;

}
