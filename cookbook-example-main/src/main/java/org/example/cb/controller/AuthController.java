package org.example.cb.controller;

import lombok.RequiredArgsConstructor;
import org.example.cb.model.dto.LoginDTO;
import org.example.cb.model.dto.AuthResponseDTO;
import org.example.cb.service.security.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("/get-token")
    public ResponseEntity<AuthResponseDTO> getToken(@RequestBody LoginDTO loginDTO) {
        return ResponseEntity.ok(authService.getAccessToken(loginDTO));
    }

    @PostMapping("/refresh-token")
    public ResponseEntity<AuthResponseDTO> getToken(@RequestBody String refreshToken) {
        return ResponseEntity.ok(authService.refreshToken(refreshToken));
    }

}
