package org.example.cb.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import lombok.RequiredArgsConstructor;
import org.example.cb.feign.CbrClient;
import org.example.cb.model.dto.cbr.CurrencyRatesDTO;
import org.example.cb.service.CurrencyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.StringReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/course")
@RequiredArgsConstructor
@SecurityRequirement(name = "api")
public class CourseController {

    private final CbrClient cbrClient;
    private final CurrencyService currencyService;

    @GetMapping("/get")
    public ResponseEntity<CurrencyRatesDTO> getCourse() throws JAXBException {
        String formattedDate = LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)); // 17/02/23
        String response = cbrClient.getExchangeRate(formattedDate);

        JAXBContext jaxbContext = JAXBContext.newInstance(CurrencyRatesDTO.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(response);
        CurrencyRatesDTO unmarshal = (CurrencyRatesDTO) unmarshaller.unmarshal(reader);

        return ResponseEntity.ok(unmarshal);
    }

    @GetMapping("/get/{date}")
    public ResponseEntity<CurrencyRatesDTO> getCourse(@PathVariable("date") LocalDate date) throws JAXBException {
        String formattedDate = date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT));
        String response = cbrClient.getExchangeRate(formattedDate);

        JAXBContext jaxbContext = JAXBContext.newInstance(CurrencyRatesDTO.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(response);
        CurrencyRatesDTO unmarshal = (CurrencyRatesDTO) unmarshaller.unmarshal(reader);

        return ResponseEntity.ok(unmarshal);
    }

    @GetMapping("/loading-currency-rate")
    public ResponseEntity<Void> loadingCurrencyRate(
            @RequestParam(value = "date", required = false) Optional<LocalDate> date) throws JAXBException {
        currencyService.loadingCurrencyRate(date);
        return ResponseEntity.noContent().build();
    }
}
