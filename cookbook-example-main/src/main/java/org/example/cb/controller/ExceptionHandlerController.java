package org.example.cb.controller;

import liquibase.integration.commandline.Main;
import org.example.cb.exception.MainApplicationException;
import org.example.cb.model.dto.ErrorResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorResponseDTO> handleRuntimeException(RuntimeException e) {

        ErrorResponseDTO dto = ErrorResponseDTO.builder()
                .message(e.getMessage())
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .build();

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(dto);
    }

    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity<ErrorResponseDTO> handleAccessDeniedException(Exception e) {

        ErrorResponseDTO dto = ErrorResponseDTO.builder()
                .message("Access denied message here")
                .status(HttpStatus.FORBIDDEN)
                .build();

        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(dto);
    }

    @ExceptionHandler(MainApplicationException.class)
    public ResponseEntity<ErrorResponseDTO> handleRuntimeException(MainApplicationException e) {

        ErrorResponseDTO dto = ErrorResponseDTO.builder()
                .message(e.getMessage())
                .status(e.getStatus())
                .build();

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(dto);
    }




}
