package org.example.cb.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.example.cb.model.dto.UserDTO;
import org.example.cb.model.entity.User;
import org.example.cb.model.mapper.UserMapper;
import org.example.cb.service.UserService;
import org.mapstruct.factory.Mappers;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@SecurityRequirement(name = "api")
public class UserController {

    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);
    private final UserService userService;

    @GetMapping("/get")
    public ResponseEntity<List<UserDTO>> getUsers() {
        List<User> userList = userService.getAllUsers();
        return ResponseEntity.ok(userMapper.toDtoList(userList));
    }

    @PostMapping("/create")
    public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO) {
        User user = userService.saveUser(userMapper.fromDto(userDTO));
        return ResponseEntity.ok(userMapper.toDto(user));
    }
}
