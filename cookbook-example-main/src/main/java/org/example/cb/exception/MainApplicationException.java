package org.example.cb.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public abstract class MainApplicationException extends RuntimeException {

    private final String message;

    private final HttpStatus status;

    public MainApplicationException(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }
}
