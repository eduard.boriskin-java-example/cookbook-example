package org.example.cb.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "get-exchange", url = "https://cbr.ru/")
public interface CbrClient {

    @RequestMapping(method = RequestMethod.GET,
            value = "/scripts/XML_daily.asp?date_req={date}",
            produces = MediaType.APPLICATION_XML_VALUE)
    String getExchangeRate(@PathVariable("date") String date);

}
