package org.example.cb.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

import static org.example.cb.component.constant.DatabaseConstants.*;

@Getter
@Setter
@Entity
@Table(schema = SCHEMA_COOKBOOK, name = TABLE_CURRENCY)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Currency {

    @Id
    @Column(name = ID)
    private int id;

    @Column(name = CODE)
    private String code;

    @Column(name = NAME)
    private String name;

    @Column(name = LOAD)
    private Boolean load;

}
