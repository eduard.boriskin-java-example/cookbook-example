package org.example.cb.model.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

import static org.example.cb.component.constant.DatabaseConstants.*;

@Getter
@Setter
@Entity
@Table(schema = SCHEMA_COOKBOOK, name = CURRENCY_RATE_SEQ)
public class CurrencyRate {

    @Id
    @SequenceGenerator(name = CURRENCY_RATE_GEN, schema = SCHEMA_COOKBOOK, sequenceName = CURRENCY_RATE_SEQ)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = CURRENCY_RATE_GEN)
    @Column(name = ID)
    private Long id;

    @OneToOne
    @JoinColumn(name = CURRENCY_ID)
    private Currency currency;

    @Column(name = RATE)
    private Long rate;

    @Column(name = DATE)
    private LocalDate localDate;


}
