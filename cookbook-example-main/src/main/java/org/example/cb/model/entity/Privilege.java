package org.example.cb.model.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

import static org.example.cb.component.constant.DatabaseConstants.*;

@Getter
@Setter
@Entity
@Table(schema = SCHEMA_COOKBOOK, name = TABLE_PRIVILEGES)
public class Privilege implements GrantedAuthority {

    @Id
    @SequenceGenerator(name = GEN_PRIVILEGES, schema = SCHEMA_COOKBOOK, sequenceName = SEQ_PRIVILEGES)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GEN_PRIVILEGES)
    @Column(name = ID)
    private long id;

    @Column(name = NAME)
    private String name;

    @ManyToMany(mappedBy = PRIVILEGES)
    private Collection<Role> roles;

    @Override
    public String getAuthority() {
        return getName();
    }
}
