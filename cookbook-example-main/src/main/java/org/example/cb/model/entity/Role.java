package org.example.cb.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Collection;

import static org.example.cb.component.constant.DatabaseConstants.*;

@Getter
@Setter
@Entity
@Table(schema = SCHEMA_COOKBOOK, name = TABLE_ROLE)
@ToString(onlyExplicitlyIncluded = true)
public class Role {

    @Id
    @SequenceGenerator(name = GEN_ROLES, schema = SCHEMA_COOKBOOK, sequenceName = SEQ_ROLES)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GEN_ROLES)
    @Column(name = ID)
    private long id;

    @ToString.Include
    @Column(name = NAME)
    private String name;

    @ManyToMany
    @JsonBackReference
    @JoinTable(
            schema = SCHEMA_COOKBOOK,
            name = USERS_ROLES,
            joinColumns = @JoinColumn(
                    name = ROLE_ID, referencedColumnName = ID),
            inverseJoinColumns = @JoinColumn(
                    name = USER_ID, referencedColumnName = ID))
    private Collection<User> users;

    @ManyToMany
    @JsonBackReference
    @JoinTable(
            schema = SCHEMA_COOKBOOK,
            name = ROLES_PRIVILEGES,
            joinColumns = @JoinColumn(
                    name = ROLE_ID, referencedColumnName = ID),
            inverseJoinColumns = @JoinColumn(
                    name = PRIVILEGE_ID, referencedColumnName = ID))
    private Collection<Privilege> privileges;

}
