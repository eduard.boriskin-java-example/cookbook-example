package org.example.cb.model.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;

import java.util.Collection;

import static org.example.cb.component.constant.DatabaseConstants.*;

@Getter
@Setter
@Entity
@BatchSize(size = SIZE_BATCH)
@Table(schema = SCHEMA_COOKBOOK, name = TABLE_USERS)
public class User {

    @Id
    @SequenceGenerator(name = GEN_USERS, schema = SCHEMA_COOKBOOK, sequenceName = SEQ_USERS)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GEN_USERS)
    @Column(name = ID)
    private long id;

    @Column(name = USERNAME)
    private String username;

    @Column(name = PASSWORD)
    private String password; // TODO: 03.02.2023 change to byte[]

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            schema = SCHEMA_COOKBOOK,
            name = USERS_ROLES,
            joinColumns = @JoinColumn(
                    name = USER_ID, referencedColumnName = ID),
            inverseJoinColumns = @JoinColumn(
                    name = ROLE_ID, referencedColumnName = ID))
    private Collection<Role> roles;
}
