package org.example.cb.model.mapper;

import org.example.cb.model.dto.UserDTO;
import org.example.cb.model.entity.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDTO toDto(User user);

    User fromDto(UserDTO dto);

    List<UserDTO> toDtoList(List<User> userList);

}
