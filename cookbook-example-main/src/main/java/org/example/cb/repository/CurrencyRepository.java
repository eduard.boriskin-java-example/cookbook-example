package org.example.cb.repository;

import org.example.cb.model.entity.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Transactional
public interface CurrencyRepository extends JpaRepository<Currency, Integer> {


    @Query(value = "SELECT c.code FROM Currency c WHERE c.load")
    Set<String> findAllForDownloadable();


}
