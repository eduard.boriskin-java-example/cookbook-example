package org.example.cb.service;

import org.example.cb.model.dto.cbr.CurrencyDTO;
import org.example.cb.model.entity.CurrencyRate;

public interface CurrencyRateService {
    void saveRate(CurrencyRate rate);

    void saveRate(CurrencyDTO currencyDTO);
}
