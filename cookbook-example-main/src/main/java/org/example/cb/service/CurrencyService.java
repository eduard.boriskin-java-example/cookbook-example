package org.example.cb.service;

import jakarta.xml.bind.JAXBException;

import java.time.LocalDate;
import java.util.Optional;

public interface CurrencyService {
    void loadingCurrencyRate(Optional<LocalDate> date) throws JAXBException;
}
