package org.example.cb.service;

import org.example.cb.model.entity.Privilege;

public interface PrivilegeService {
    Privilege savePrivilege(Privilege privilege);

    Privilege getByName(String name);
}
