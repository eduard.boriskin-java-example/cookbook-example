package org.example.cb.service;

import org.example.cb.model.entity.Role;

public interface RoleService {
    Role saveRole(Role role);
}
