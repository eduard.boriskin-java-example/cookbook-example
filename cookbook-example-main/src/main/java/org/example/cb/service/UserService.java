package org.example.cb.service;

import org.example.cb.model.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllUsers();

    User saveUser(User user);

    Optional<User> findUserByUsername(String username);
}
