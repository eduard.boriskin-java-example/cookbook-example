package org.example.cb.service.impl;

import lombok.RequiredArgsConstructor;
import org.example.cb.model.dto.cbr.CurrencyDTO;
import org.example.cb.model.entity.CurrencyRate;
import org.example.cb.repository.CurrencyRateRepository;
import org.example.cb.service.CurrencyRateService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CurrencyRateServiceImpl implements CurrencyRateService {

    private final CurrencyRateRepository repository;

    @Override
    public void saveRate(CurrencyRate rate) {
        repository.save(rate);
    }

    @Override
    public void saveRate(CurrencyDTO currencyDTO) {

    }

}
