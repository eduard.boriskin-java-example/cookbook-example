package org.example.cb.service.impl;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import lombok.RequiredArgsConstructor;
import org.example.cb.feign.CbrClient;
import org.example.cb.model.dto.cbr.CurrencyDTO;
import org.example.cb.model.dto.cbr.CurrencyRatesDTO;
import org.example.cb.model.entity.Currency;
import org.example.cb.repository.CurrencyRepository;
import org.example.cb.service.CurrencyRateService;
import org.example.cb.service.CurrencyService;
import org.springframework.stereotype.Service;

import java.io.StringReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final CbrClient cbrClient;
    private final CurrencyRepository repository;

    private final CurrencyRateService currencyRateService;

    @Override
    public void loadingCurrencyRate(Optional<LocalDate> date) throws JAXBException {
        Set<String> downloadableCurrencyCode = repository.findAllForDownloadable();
        executeCurrencyRate(date.orElse(LocalDate.now())).getCurrencies().stream()
                .filter(currencyDTO -> downloadableCurrencyCode.contains(currencyDTO.getCharCode()))
                .forEach(currencyRateService::saveRate);
    }



    private CurrencyRatesDTO executeCurrencyRate(LocalDate date) throws JAXBException {
        String formattedDate = date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT));
        String response = cbrClient.getExchangeRate(formattedDate);

        JAXBContext jaxbContext = JAXBContext.newInstance(CurrencyRatesDTO.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(response);
        return  (CurrencyRatesDTO) unmarshaller.unmarshal(reader);
    }
}
