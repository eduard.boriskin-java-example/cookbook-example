package org.example.cb.service.impl;

import lombok.RequiredArgsConstructor;
import org.example.cb.model.entity.Privilege;
import org.example.cb.repository.PrivilegeRepository;
import org.example.cb.service.PrivilegeService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PrivilegeServiceImpl implements PrivilegeService {

    private final PrivilegeRepository repository;

    @Override
    public Privilege savePrivilege(Privilege privilege) {
        return repository.save(privilege);
    }

    @Override
    public Privilege getByName(String name) {
        return repository.findByName(name);
    }

}
