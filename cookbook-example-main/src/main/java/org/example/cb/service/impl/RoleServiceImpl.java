package org.example.cb.service.impl;

import lombok.RequiredArgsConstructor;
import org.example.cb.model.entity.Role;
import org.example.cb.repository.RoleRepository;
import org.example.cb.service.RoleService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository repository;

    @Override
    public Role saveRole(Role role) {
        return repository.save(role);
    }

}
