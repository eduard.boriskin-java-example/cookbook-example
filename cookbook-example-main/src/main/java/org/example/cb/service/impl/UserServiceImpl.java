package org.example.cb.service.impl;

import lombok.RequiredArgsConstructor;
import org.example.cb.model.entity.User;
import org.example.cb.repository.UserRepository;
import org.example.cb.service.UserService;
import org.example.cb.service.security.SecurityService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final SecurityService securityService;

    @Override
    public List<User> getAllUsers() {
        return repository.findAll();
    }

    @Override
    public User saveUser(User user) {
        user.setPassword(securityService.hashPwd(user.getPassword()));
        return repository.save(user);
    }

    @Override
    public Optional<User> findUserByUsername(String username) {
        return repository.findByUsername(username);
    }

}
