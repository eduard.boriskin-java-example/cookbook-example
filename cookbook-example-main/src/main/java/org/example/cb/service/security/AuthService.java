package org.example.cb.service.security;

import org.example.cb.model.dto.LoginDTO;
import org.example.cb.model.dto.AuthResponseDTO;
import org.springframework.security.core.Authentication;

public interface AuthService {

    AuthResponseDTO getAccessToken(LoginDTO loginDTO);

    AuthResponseDTO refreshToken(String refreshToken);

    Authentication getAuthentication(String token);


}
