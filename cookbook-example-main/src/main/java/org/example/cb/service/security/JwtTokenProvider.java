package org.example.cb.service.security;

import io.jsonwebtoken.Claims;
import lombok.NonNull;
import org.example.cb.model.entity.User;
import org.example.cb.model.dto.AuthResponseDTO;

public interface JwtTokenProvider {
    String generateAccessToken(User user);

    String generateRefreshToken(User user);

    Claims getRefreshClaims(String refreshToken);

    boolean validateAccessToken(@NonNull String accessToken);

    boolean validateRefreshToken(@NonNull String refreshToken);

    Claims getAccessClaims(@NonNull String token);

    AuthResponseDTO createAuthResponse(User user);
}
