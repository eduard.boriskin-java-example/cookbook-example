package org.example.cb.service.security;

import org.example.cb.model.entity.User;

public interface SecurityService {
    String hashPwd(String password);

    boolean checkPwd(User password, String encodedPassword);
}
