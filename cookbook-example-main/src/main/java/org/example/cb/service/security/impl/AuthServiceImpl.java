package org.example.cb.service.security.impl;

import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.example.cb.model.dto.LoginDTO;
import org.example.cb.model.entity.User;
import org.example.cb.model.dto.AuthResponseDTO;
import org.example.cb.service.UserService;
import org.example.cb.service.security.AuthService;
import org.example.cb.service.security.JwtTokenProvider;
import org.example.cb.service.security.SecurityService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserService userService;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserDetailsService userDetailsService;
    private final SecurityService securityService;

    @Override
    public AuthResponseDTO getAccessToken(LoginDTO loginDTO) {
        Optional<User> user = userService.findUserByUsername(loginDTO.getUsername());
        if (user.isEmpty() || !securityService.checkPwd(user.get(), loginDTO.getPassword())) {
            throw new RuntimeException("User not found");
        }
        return jwtTokenProvider.createAuthResponse(user.get());
    }

    @Override
    public AuthResponseDTO refreshToken(String refreshToken) {
        if (jwtTokenProvider.validateRefreshToken(refreshToken)) {
            final Claims claims = jwtTokenProvider.getRefreshClaims(refreshToken);
            final String username = claims.getSubject();

            User user = userService.findUserByUsername(username)
                    .orElseThrow(() -> new RuntimeException("User not found"));
            return jwtTokenProvider.createAuthResponse(user);
        }
        throw new RuntimeException("Refresh token not valid");
    }

    @Override
    public Authentication getAuthentication(String token) {
        Claims claims = jwtTokenProvider.getRefreshClaims(token);

        UserDetails userDetails = this.userDetailsService.loadUserByUsername(claims.getSubject());
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }


}
