package org.example.cb.service.security.impl;

import lombok.RequiredArgsConstructor;
import org.example.cb.model.entity.User;
import org.example.cb.service.UserService;
import org.example.cb.util.SecurityUtil;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class DatabaseUserDetailsManager implements UserDetailsService {

    private final UserService userService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findUserByUsername(username).orElseThrow(() -> new RuntimeException("User not found"));
        return getDetails(user);
    }

    private UserDetails getDetails(User user) {
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                SecurityUtil.mapRolesAuthorities(user.getRoles()));
    }

}
