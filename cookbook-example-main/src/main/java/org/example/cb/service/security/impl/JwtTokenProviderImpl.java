package org.example.cb.service.security.impl;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.example.cb.config.credential.SecurityCredential;
import org.example.cb.model.entity.User;
import org.example.cb.model.dto.AuthResponseDTO;
import org.example.cb.service.security.JwtTokenProvider;
import org.example.cb.util.SecurityUtil;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.security.Key;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Service
@Slf4j
public class JwtTokenProviderImpl implements JwtTokenProvider {

    private final SecretKey jwtAccessSecret;
    private final SecretKey jwtRefreshSecret;

    private final int tokenLifespan;
    private final int tokenRefreshLifespan;

    public JwtTokenProviderImpl(SecurityCredential credential) {
        jwtAccessSecret = Keys.hmacShaKeyFor(Decoders.BASE64.decode(credential.getJwtSecret()));
        jwtRefreshSecret = Keys.hmacShaKeyFor(Decoders.BASE64.decode(credential.getJwtRefreshSecret()));
        tokenLifespan = credential.getTokenLifespan();
        tokenRefreshLifespan = credential.getTokenRefreshLifespan();
    }

    @Override
    public AuthResponseDTO createAuthResponse(User user) {
        return AuthResponseDTO.builder()
                .accessToken(generateAccessToken(user))
                .expiresIn(tokenLifespan)
                .refreshToken(generateRefreshToken(user))
                .refreshExpiresIn(tokenRefreshLifespan)
                .tokenType("bearer")
                .build();
    }

    @Override
    public String generateAccessToken(User user) {

        final LocalDateTime now = LocalDateTime.now();
        final Instant accessExpirationInstant = now.plusSeconds(tokenLifespan)
                .atZone(ZoneId.systemDefault()).toInstant();
        final Date accessExpiration = Date.from(accessExpirationInstant);

        return Jwts.builder()
                .setSubject(user.getUsername())
                .setExpiration(accessExpiration)
                .signWith(jwtAccessSecret)
                .claim("authorities", SecurityUtil.mapRolesAuthorities(user.getRoles()))
                .compact();
    }

    @Override
    public String generateRefreshToken(User user) {
        final LocalDateTime now = LocalDateTime.now();
        final Instant refreshExpirationInstant = now.plusSeconds(tokenRefreshLifespan)
                .atZone(ZoneId.systemDefault()).toInstant();
        final Date refreshExpiration = Date.from(refreshExpirationInstant);
        return Jwts.builder()
                .setSubject(user.getUsername())
                .setExpiration(refreshExpiration)
                .signWith(jwtRefreshSecret)
                .compact();
    }

    @Override
    public Claims getRefreshClaims(String token) {
        return getClaims(token, jwtRefreshSecret);
    }

    private Claims getClaims(String token, Key secret) {
        return Jwts.parserBuilder()
                .setSigningKey(secret)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    @Override
    public boolean validateAccessToken(@NonNull String accessToken) {
        return validateToken(accessToken, jwtAccessSecret);
    }

    @Override
    public boolean validateRefreshToken(@NonNull String refreshToken) {
        return validateToken(refreshToken, jwtRefreshSecret);
    }

    @Override
    public Claims getAccessClaims(@NonNull String token) {
        return getClaims(token, jwtAccessSecret);
    }


    private boolean validateToken(@NonNull String token, @NonNull Key secret) {
        try {
            Jwts.parserBuilder()
                    .setSigningKey(secret)
                    .build()
                    .parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            log.error("invalid token {}", e.getMessage());
        }
        return false;
    }

}
