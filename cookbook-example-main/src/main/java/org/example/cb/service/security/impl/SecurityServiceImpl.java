package org.example.cb.service.security.impl;

import lombok.RequiredArgsConstructor;
import org.example.cb.model.entity.User;
import org.example.cb.service.security.SecurityService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SecurityServiceImpl implements SecurityService {

    private final BCryptPasswordEncoder encoder;

    @Override
    public String hashPwd(String password) {
        return encoder.encode(password);
    }

    @Override
    public boolean checkPwd(User user, String password) {
        return encoder.matches(password, user.getPassword());
    }
}
