package org.example.cb.util;

import lombok.experimental.UtilityClass;
import org.example.cb.model.entity.Role;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.stream.Collectors;

@UtilityClass
public class SecurityUtil {

    public Collection<? extends GrantedAuthority> mapRolesAuthorities(Collection<Role> roles) {
        return roles.stream().flatMap(role -> role.getPrivileges().stream()).collect(Collectors.toList());
    }

}

