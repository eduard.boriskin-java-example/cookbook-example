CREATE SCHEMA cookbook;

-- user

CREATE SEQUENCE cookbook.users_seq
    START WITH 10
    INCREMENT 10;

CREATE TABLE cookbook.users
(
    id       long    NOT NULL DEFAULT nextval('cookbook.users_seq') PRIMARY KEY,
    username varchar NOT NULL,
    password varchar NOT NULL

);
CREATE UNIQUE INDEX idx_users_username ON cookbook.users (username);

-- privilege

CREATE SEQUENCE cookbook.privileges_seq;
CREATE TABLE cookbook.privileges
(
    id   long    NOT NULL DEFAULT nextval('cookbook.privileges_seq') PRIMARY KEY,
    name varchar NOT NULL
);
CREATE UNIQUE INDEX idx_privileges_name ON cookbook.privileges (name);

-- role

CREATE SEQUENCE cookbook.roles_seq;
CREATE TABLE cookbook.roles
(
    id   long    NOT NULL DEFAULT nextval('cookbook.roles_seq') PRIMARY KEY,
    name varchar NOT NULL
);

-- user_roles
CREATE TABLE cookbook.users_roles
(
    id      serial PRIMARY KEY,
    user_id long,
    role_id long
);

-- roles_privileges
CREATE TABLE cookbook.roles_privileges
(
    id           serial PRIMARY KEY,
    role_id      long,
    privilege_id long
);

CREATE TABLE cookbook.currency
(
    id   int      NOT NULL PRIMARY KEY,
    code varchar  NOT NULL,
    name varchar  NOT NULL,
    load smallint NOT NULL
);

CREATE SEQUENCE cookbook.currency_rate_seq;
CREATE TABLE cookbook.currency_rate
(
    id          long NOT NULL DEFAULT nextval('cookbook.currency_rate_seq') PRIMARY KEY,
    currency_id long NOT NULL,
    date        date NOT NULL,
    rate        long NOT NULL
)

