-- init ddl

INSERT INTO cookbook.privileges (name)
VALUES ('full'),
       ('base');

INSERT INTO cookbook.currency (id, code, name, load)
VALUES (643, 'RUB', 'Российский рубль', 0),
       (840, 'USD', 'Доллар США', 1);